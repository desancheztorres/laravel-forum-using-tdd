<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Http\Requests\Threads\StoreThreadRequest;
use App\Thread;
use App\Filters\ThreadFilters;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ThreadsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * @param Request $request
     * @param Channel $channel
     * @param ThreadFilters $filters
     * @return Factory|View
     */
    public function index(Request $request, Channel $channel, ThreadFilters $filters) {

        $threads = $this->getThreads($channel, $filters);

        return view('threads.index', compact('threads'));
    }

    /**
     * @param Channel $channel
     * @param Thread $thread
     * @return Factory|View
     */
    public function show(Channel $channel, Thread $thread) {
        return view('threads.show', compact('thread'));
    }

    public function create() {
        return view('threads.create');
    }

    public function store(StoreThreadRequest $request) {
        $thread = Thread::create([
            'user_id' => auth()->id(),
            'channel_id' => $request->channel_id,
            'title' => $request->title,
            'body' => $request->body
        ]);

        return redirect($thread->path());
    }

    /**
     * @param Channel $channel
     * @param ThreadFilters $filters
     * @return mixed
     */
    public function getThreads(Channel $channel, ThreadFilters $filters)
    {
        $threads = Thread::latest()->filter($filters);

        if ($channel->exists) {
            $threads->where('channel_id', $channel->id);
        }

        $threads = $threads->get();
        return $threads;
    }
}
