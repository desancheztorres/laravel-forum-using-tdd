<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Http\Requests\Replies\StoreReplyRequest;
use App\Thread;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class RepliesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param StoreReplyRequest $request
     * @param Channel $channel
     * @param Thread $thread
     * @return RedirectResponse
     */
    public function store(StoreReplyRequest $request, Channel $channel, Thread $thread) {
        $thread->addReply([
            'body' => $request->body,
            'user_id' => auth()->id(),
        ]);

        return back();
    }
}
